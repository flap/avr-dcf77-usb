Note: English language variant see below.

DCF77 USB-Empfänger: Hard- und Firmware
=======================================

Was am Ende rauskommen soll
---------------------------

![Fertiges Gerät](/files/usb_dcf77_small.jpg)

Dargestellt ist links oben die Auswerteelektronik und unten recht der
DCF77-Signalempfänger

Planung ersetzt Zufall durch Irrtum
-----------------------------------

Am Anfang war geplant, im Microcontroller die komplette Uhrenfunktion
unterzubringen. Da ich prinzipiell solch eine DCF77-Uhr bereits gebaut und
programmiert hatte, erschien mir das als nicht sonderlich schwierig.

Allerdings zeigen die anderen Uhren die empfangene Zeit mittels LEDs oder andern
Anzeigen direkt für das menschliche Auge an. Dieses Projekt aber soll die Zeit
an einen Rechnerverbund weiterleiten. Und dabei ergeben sich ganz andere
Anforderungen an die Qualität der Zeit.

Zu allem Übel kommt noch hinzu, dass man immer damit rechnen muß, daß die
Empfangsqualität nicht dauerhaft gesichert und auch nicht immer von hoher
Qualität ist.

Gemäß dem Spruch "Bei schönem Wetter kann jeder Segeln", habe ich dann
verschiedene Verfahren ausprobiert, um auch aus einem schlechten, nur hin und
wieder verfügbaren DCF77-Signal eine brauchbare Zeitreferenz zu erzeugen. Dabei
kam es zu den aberwitzigsten Situationen, in denen der Programm-Code Amok lief,
weil ich wieder mal irgendeine blöde Nebenbedingung vergessen hatte. So wuchs
der Code immer mehr an, um die vielen Ausnahmebehandlungen abzufangen und unter
allen Umständen ein zeitkontinuierliches, jitterfreies Zeitsignal zu liefern.

Als der Code dann etwa 90% des verfügbaren Flashes beanspruchte und immer noch
die Routinen zur Behandlung der Schaltjahre, Schaltsekunden und
Sommerzeitumstellung nicht enthielt (die den Fall abdecken sollten, daß
ausgerechnet in der Stunde oder Minute vor dem besonderen Ereignis kein
brauchbarer Empfang herrscht) habe ich aufgegeben.

Übrig geblieben ist nun die reine Empfangsfunktion. Die eigentliche
DCF77-Dekodierung muss nun doch der angeschlossene Rechner erledigen. Dessen
Resourcen sind auch nicht so begrenzt wie in meinem Mikrocontroller.

Hauptfunktion des Controllers ist es nun, aus dem DCF77-Signal die richtigen
Schlüsse zu ziehen. D.h. auch aus einem gestörten oder teilweise nicht verfügbaren
Signal etwas brauchbares zu erzeugen, was einem hilft, über diese Phasen ohne
neue DCF77-Daten hinwegzukommen. Das ist insbesondere die Bereitstellung:

  - eines hochgenauen Sekundenintervals
  - eines hochgenauen Sekundensignals

Klingt beides fast gleich. Das eine soll aber helfen, daß die Zeitreferenz über
einen längeren Zeitraum nicht wegdriftet. Das andere ist die Phasenlage des
Startzeitpunkts einer jeden Sekunde. Hört sich alles trivial an, weil beides das
DCF77-Signal liefert. Sicher, das DCF77-Signal schon, aber nicht jeder
DCF77-Empfänger! Der Spaß beginnt, wenn das DCF77-Signal mit einem Jitter von
±20ms vom Empfänger wiedergegeben wird. Da kommen am Ende die herrlichsten
Effekte raus, nur keine brauchbare Uhrzeit...

Das folgende Bild zeigt die mit dem internen Timer gemessenen DCF77-Intervalle
des Empfängers. Die CPU und damit der Timer werden mit einer 12,0 MHz Referenz
betrieben und somit sollte eine Sekunde einen Wert von 46.875 Zählern liefern.
Tätsächlich sieht das aber ganz anders aus. Die gelben Kreuze zeigen jeweils
einen gemessenen Wert an (Messzeit war 3.000 Sekunden):

![Jitter des Empfängers](/files/dcf77_rec_intervals_mini.png)

Die rote Linie zeigt das, was die Routine aus den Daten als Interval errechnet.
Es ist keine Linie, weil ich den Abstand zu der errechneten Sollzeit mit
darstelle. Mein 12,0 MHz Quarz liegt nicht ganz bei 12,0, sondern leicht darüber.
Somit sind die gemessenen Intervalle alle etwas größer als die errechneten
46.875 Zähler (meist so um die 46.880 Zähler).

Nach langem Probieren stellte sich heraus, dass man ein brauchbares
Sekundeninterval ermitteln kann, wenn man etwa 300 gemessene DCF77-Signal-Intervalle
mittelt. Dann stellt sich ein sehr konstanter Wert ein. Das aber auch nur dann,
wenn man vorher die vermeintlich unbrauchbaren gemessenen Intervalle aussortiert.
Manchmal kommt es zu einem Sturm an Flanken auf der Signalleitung des Empfängers.
Das muß man sauber aussortieren, sonst ist alles wieder zum Teufel. Zum Glück
hilft hier die Hardware des Microcontrollers. Die Zeitpunkte der Flanken müssen
nicht mühsam in Software ermittelt werden oder über Interrupte, sondern die Hardware
selber speichert im Moment der Flanke den Wert eines frei laufenden Zählers und
signalisiert durch ein Bit dieses Ereignis. Somit ist es möglich, in aller Ruhe
diese Ereignisse auszuwerten, ohne Stress und hohe Anforderung an irgendwelches
Echtzeitverhalten. Damit erhält man eine sehr präzise Beschreibung des Signalverlaufs
und kann daraus alle erforderlichen Schlüsse ziehen. Beispielsweise ob ein
Intervall zwischen zwei gleichen Flanken in ein 1-Sekunden-Raster passt oder ob
es sich eher um einen Störimpuls handelt, den man verwerfen muss.

Nach dem Ermitteln der "perfekten" Sekunde, muss in einem nachgeschalteten
Schritt die Phasenlage der internen Zeitreferenz an das des DCF77-Signals
angeglichen werden. Wohlgemerkt an ein Signal, welches mit ±20ms fröhlich jittert.
Auch hier ist mir nichts anderes eingefallen, als den gemessenen Jitter über einige
Intervalle hinweg zu mitteln und dann die interne Zeitreferenz an den errechneten
Ort zu verlegen. Danach folgt der Algorithmus dann in sehr kleinen Sprüngen dem
Jitter, folgt somit durch einen Tiefpass hindurch der DCF77-Signalvorgabe. Fehlt
das DCF77-Signal vollständig bleibt die Phase einfach stehen und dank der
abgeglichenen Intervall-Länge sollte das Sekundensignal auch über einen längeren
Zeitraum hinweg stabil genug bleiben.

Soweit die Theorie. Es stellte sich aber heraus, dass wenn die Phasenabweichung
zwischen dem DCF77-Signal und dem internen Referenz-Takt nahe 0 ist (also
ausgeregelt), die Software wiederum Amok läuft und ein ganz merkwürdiges Verhalten
an den Tag legte. Nach langer Suche fand ich heraus, das in diesem Moment die
Software nicht mehr zweifelsfrei feststellen konnte, welches der beiden Signale
nun vor- bzw. nacheilt und somit ständig die falschen Entscheidungen traf, wohin
denn nun die Phase verschoben werden müsste. Schon wieder den Zufall durch Irrtum
ersetzt! Geholfen hat am Ende nur, die Phasenlage deutlich zu verschieben und
dauerhaft auf konstanten Abstand zu regeln. Damit kommen die Ereignisse in einer
definierten Reihenfolge, die Phasenlage kann damit zweifelsfrei ermittelt und somit
immer die richtigen Schlüsse gezogen werden. Die konstante Phasenabweichung kann
beim Report an den Host nachträglich wieder rausgerechnet werden.

Anbindung an den HOST Rechner
-----------------------------

In einem anderen kleinen Projekt, die empfangene DCF77-Zeit in einen Rechner
einzuspeisen, kam seinerzeit noch eine RS232-Schnittstelle zum Einsatz. Solche
Schnittstellen sterben heute immer mehr aus, waren also in diesem Projekt hier
keine Option. Der USB ist stattdessen heute das Mittel der Wahl. Da ich aber
nun nicht gleich auch einen USB-tauglichen Microcontroller einsetzen wollte,
habe ich wieder auf V-USB zurückgegriffen. Mit V-USB wird ein einfacher
ATmega-Microcontroller mit ein paar extern Bauteilen zu einem USB-Lowspeed-Device.
Da in diesem Projekt nur sehr wenig Daten übertragen werden müssen, reicht
diese Funktionalität vollkommen aus.

Hardware
--------

Die Bauteile sind in einer sehr überschaubaren Anzahl vorhanden und das Löten
dauert rund eine halbe Stunde. Okay, okay, es sind SMD-Bauteile beteiligt und
die sind nicht jedermanns Sache. Aber hey! DIL ist out. Mit etwas Ruhe, Geduld
und viel "Solderwick" sollte sich auch SMD löten lassen. Immerhin sind die
Widerstände und Kapazitäten 0805er Bauweise. Also eigentlich riesig, oder?
Nur die CPU ist etwas kritisch. Aber mir hilft in so einem Fall immer viel
Licht, ein sehr feiner Lötkolben und eine dicke Lupe.

![Oberseite, CAD](/files/layout-top-side_small.png)
![Unterseite, CAD](/files/layout-bottom-side_small.png)

So sieht es aufgebaut aus
-------------------------

![Fertiger Aufbau](/files/usb_dcf77_small.jpg)

Schaltplan
----------

Die Schaltpläne liegen im PostScript-Format vor:

[Hauptseite](/files/main_page.ps)
[CPU-Seite](/files/cpu_page.ps)
[USB-Seite](/files/usb_page.ps)

Layout
------

Hier die zugehörige EAGLE-Layout-Datei

[Layout](/files/avr-dcf77.brd)

Firmware
--------

Was gibt es zur Firmware zu sagen? Nicht allzu viel, aber:

 - Man sollte in selber geschriebenen Interruptroutinen in Assembler nicht
   vergessen, das Statusregister zu sichern
 - Man sollte ebenfalls nicht vergessen, konkurrierende Zugriffe auf interne
   16 Bit Register zu verhindern

Beides hat mich rund 6 Wochen beschäftigt, weil es nur sporadisch zu einem
Fehlverhalten des Programms führte. Da sucht man sich echt 'nen Wolf...

Da die V-USB-Routinen recht harte Echtzeitanforderungen haben, benutze ich fast
keine Interrupte. Nur das atomare Auslesen der Referenz-Sekunde verwendet noch
einen. Damit der wiederum die V-USB-Routinen nicht stört, habe ich ihn in
Assembler realisiert und extrem kurz gemacht.

Alles andere läuft in einer Endlosschleife und einer Zustandsmaschine und fragt
ständig die diversen Statusbits ab. Hier kommt es einem zugute, dass der Atmega
über brauchbare Hardware verfügt die einem viel Arbeit abnimmt.

Mitten rein in diese Endlosschleife schlägt immer wieder der USB, also Anfragen
vom Host. Da die Uhrenfunktion aber Dank Hardwareunterstützung zeitunkritisch
ist, stört das nicht weiter.

Pro Anfrage des Hosts meldet die Uhr zwei bis dahin aufgelaufene Ereignisse:
Start der Referenz-Sekunde und das letzte empfangene Bit aus dem DCF77-Datenstrom.
Da diese beiden Ereignisse zeitlich nicht zusammenfallen (das Bit kennt man
immer erst, wenn das gerade aktive DCF77-Signal endet) sind pro Sekunde
mindestens zwei Anfragen vom Host zu stellen. Die Firmware puffert keines dieser
Ereignisse, sondern überschreibt das bisherige Ereignis gnadenlos mit den
nächsten Daten. Immerhin meldet die Firmware in so einem Fall, dass mindestens
ein DCF77-Bit verpasst wurde.

Jede Antwort an den Host besteht aus 8 Bytes und hat den folgenden Inhalt:

| Name         | Bits | Bedeutung                                                                           |
| :----------: | :--: | :----------------------------------------------------------------------------------
|    flags     |  8   | Bit 4: 1 = eine neue Sekunde seit der letzten Abfrage hat begonnen                  |
|              |      | Bit 1: 1 = mindestens ein Bit des DCF77-Datenstroms ist verloren                    |
|              |      | Bit 0: 1 = der Wert in bit (DCF77 bit) ist gültig                                   |
|     bit      |  8   | das zuletzt empfangene Bit aus dem DCF77 Datenstrom, wenn das flags Bit 0 = 1 ist   |
|              |      | - 0: empfangen wurde ein 0 Bit                                                      |
|              |      | - 1: empfangen wurde ein 1 Bit                                                      |
|              |      | - 8: empfangen wurde ein SYNC                                                       |
|              |      | - 15: empfangen wurde ein ungültiges Bit                                            |
| stamp_offset |  16  | Zählerdifferenz zum Beginn der laufenden Sekunde                                    |
| cur_phase    |  16  | Aktuelle Phase zwischen dem Startzeitpunkt der Referenz-Sekunde und dem DCF77-Signal|
| cur_interval |  16  | Timer-Zähler für eine Sekunde                                                       |

Das Bit 4 in flags ist notwendig, weil es zu dem Fall kommen kann, dass der
Beginn einer neuen Sekunde bereits stattgefunden hat, jedoch wegen der laufenden
USB-Abfrage das Ereignis noch nicht in den erzeugten Report eingeflossen ist.
In diesem Fall kann der Host aus stamp_offset und cur_interval erkennen, dass
eine neue Sekunde bereits begonnen hat.

Wie das alles gemeint ist::

     ---------------------------------------------- time flow -------------------------------------------->

                |<----------- cur_interval (equal to one second) ----------------->|
      ref ------|------------------------------------------------------------------|-------------------------
      dcf ------------------------------------|--------------------------------------------------------------
                                              |---------- cur_phase -------------->|--- stamp_offset ---->|
                                                                                                          ^
                                                      this is the point of time we got in the report _____|

  - ref = Ereignisse der internen Referenz-Sekunde
  - dcf = Ereignisse des DCF-Signals
  - Die tatsächliche Zeit ist also cur_phase + stamp_offset, denn die interne
    Referenzzeit eilt um cur_phase dem DCF-Signal nach.

Wie wird der Zeitpunkt ermittelt, der in stamp_offset geliefert wird::

     USB  _____________________XXXXXXXXXXXXXXX_XX_________________________
     read _______________________XX_______________________________________
                               |<-------------->| ~1.08ms
                             ->|-|<-150us...250us
                               ->|-|<- ~150us to generate the answer

D.h. die gesamte Übertragung (=Aktivität auf dem USB) dauert etwa 1ms. Etwa
150µs...250µs nach dem Beginn der Aktivität auf dem USB wird intern begonnen die
Anwort zu generieren. Das bedeutet stamp_offset referenziert diesen Zeitpunkt.
Danach braucht die Firmware noch etwa 150µs um das Datenpaket zusammen zustellen.
Der Rest der Zeit wird verbraucht, um die Daten dann an den Host zu übertragen.

Und Host-seitig?
----------------

Hier hat es sich bewährt, vor und nach der Abfrage via USB einen Zeitstempel zu
ziehen und mit der Annahme weiterzurechnen, dass *stamp_offset* von der USB-Uhr
genau in der Mitte der beiden Zeitstempel liegt. Das ist natürlich fehlerbehaftet
und dieser Fehler ist schwer zu berechnen, da er vermutlich auch noch von den
sonstigen Aktivitäten auf dem USB abhängt, wann die tatsächliche Übertragung
relativ zur Systemzeit stattgefunden hat. In meinem System hat sich der NTPD aber
nur über einen Jitter von einigen hundert Microsekunden beklagt. Für meine Zwecke
ist das genau genug. Evtl. könnte ein Kernel-Treiber genauere Zeitstempel
ermitteln, es müsste sich nur jemand hinsetzen, diesen Treiber zu schreiben
(Freiwillige vor...).

Auf der Host-Seite wird natürlich auch noch ein Programm benötigt, um die
empfangenen DCF77-Bits wieder in eine Zeit umzurechnen und diese einen NTPD
zuzuführen. Dieser Teil liegt im Repository **usb-dcf77-clockread** gleich
nebenan.

Erstellen der Firmware
----------------------

Um die Firmware zu bauen braucht man einen GNU AVR Crosscompiler. Ich habe die
GCC-Version 4.3.2 zusammen mit der AVR-C-Bibliothek 1.6.2 und den Binutils 2.19
verwendet. Weiterhin braucht man die V-USB-Quellen, die ich in der Version
20081126 verwendet habe.

Nachtrag
--------

Dieses Projekt ist aus dem Jahr 2008, V-USB gibt es immer noch, muss
aber in ein neueren Version erst angepasst werden, falls jemand die Firmware
bauen möchte. Ansonsten seht es als Anregung für eine neuzeitliche Implementierung
mit einem echten USB-fähigen Microcontroller.

Fröhliches hacken!

What the goal is
================

![Final device](/files/usb_dcf77_small.jpg)

Shown here is at top left the main DCF77 to USB converter, bottom right is the
DCF77 signal receiver

Background
----------

This project was planned as a DCF77 based clock, providing the full timebase to
a connected host. I developed other DCF77 based clocks, so, I assumed that also
this project would an easy one...

But to display a time for a human eye is much easier than using this time
reference for a set of computers.

While I worked on the firmware to ensure the time reference is of a high quality
the code increases and at the end I used 90% of the available flash space and
still many features are missed. So, at the end I gave up to bring in the whole
clock feature.

The firmware is now reduced to receive the DCF77 events only, and to decode the
encoded bits in the DCF77 data stream. Processing the DCF77 data bits is done
at host's side.

Main feature of the firmware is now to detect the DCF77 events, sorting valid
from invalid events and to provide a second reference signal. Its not an easy
task, at least with the available DCF77 receivers.

The picture below shows DCF77 interval measurements taken from my DCF77 receiver.
The processor runs at 12,0MHz, so it should show 46,875 counters per second - if
the DCF77 signal would be perfect. But it isn't. Every yellow crossing shows one
measure interval over a period of 3,000 seconds. That's the reality the firmware
must handle.

![Receiver's jitter](/files/dcf77_rec_intervals_mini.png)

The red line shows the average interval the firmware calculates from this data.
Its not a thin line because it displays the difference to the reference of
46,875 counts. It also shows my crystal is not running at 12,0 MHz. It's running
a little bit faster (results into about 46,880 countes per second).

The firmware now calculates an average interval from the previous 300 DCF77 event
intervals. But it only works, if it is able to sort out all invalid events. And
sometime there are bursts of events from the receiver. Due to the capture feature
of the processor the firmware can detect this kind of noise and prevent its
usage in the average interval calculation.

After determining the perfect second interval, the second step is to adjust the
phase of the internal time reference to the received DCF77 signal. Also here I'm
using an average phase difference calculated over a few second intervals to adjust
the phase to a fixed value. As smaller the phase difference is, the smaller the
adjustment steps are. Due to this, after a while a locked state is entered, where
only very small corrections are happend. In the absence of the DCF77 signal, there
is no further phase correction, only the perfect second interval should avoid any
drift, to keep the host in sync with the DCF77 signal.

I was not able to correct the phase to 0°. In this state the firmware wasn't
longer able to distinguish which signal lags in phase. So, it never locks. Solution
was to keep the phase difference at a fixed level. With this correction all events
occuring in a predictable order. And as this phase is known, it can be suppressed
by some simple subtractions later on.

Connection to the host
----------------------

In another small DCF77 related project I used the RS232 to forward the DCF77
signal to the host. But this kind of interface become extinct, no chance to use
it any longer. Nowaday the USB is the interface to use. But I didn't want to use
a USB microcontroller for this simple job. I used the V-USB implementation by
obdev instead. With V-USB you get a USB low speed capable controller with any
simple ATmega controller by adding a few external electronic devices. My clock
device must transmit only a few bytes per second. A low speed USB device is
able to handle this.

Hardware
--------

Only a low count of devices have to be soldered. Okay, okay, there are SMT
devices and some people do not like them. But: DIL is out. With some calm,
patience and solderwick everyone should be able to solder SMT devices. But
only the CPU is critical. All other devices are huge, aren't they?

![Top Layer, CAD](/files/layout-top-side_small.png)
![Bottom Layer, CAD](/files/layout-bottom-side_small.png)

Schematics
----------

Schematics are coming in PostScript format:

[Mainpage](/files/main_page.ps)
[CPU page](/files/cpu_page.ps)
[USB page](/files/usb_page.ps)

Layout
------

Here the corresponding EAGLE layout file

[Layout](/files/avr-dcf77.brd)

Firmware
--------

Anything else to say about the firmware? Not so much, but:

 - don't forget to save the CPU's flag register in your assembler interrupt routines
 - and also don't forget to avoid concurrent access to internal 16 bit registers

Both took me about 6 weeks working on the firmware. The errors are happend
sporadically, so very hard to detect.

The V-USB routines are very sensitive to interrupt delays. So, I mostly avoid to
do things in interrupt routines. Only the atomically reading of the reference
second event is done in an interrupt routine. Its written entirely in assembler
to keep it very short. This routine switches of the interrupts for 25 cycles only.
The remaining features are done in an endless loop, running a state machine. It
handles various hardware flags to do its work.

While the endless loop is running, USB events may happen. But they do not disturb
any clock feature, due to many time critical things are done in hardware.

Every query the host sends to the clock, it responds with up to two events: The
begin of the reference second and the last received data bit from the DCF77
stream. Both events are happend at different points of time, so, the host must
send at least two queries per second to not miss any of these events. If it don't
do so, DCF77 bits can be missed. The clock reports such a lost with a flag bit
(but only reports the last received bit).

Every query will be answered by the clock with an 8 byte report:

| Name         | Bits | M  eaning                                                                   |
| :----------: | :--: | :--------------------------------------------------------------------------
|    flags     |   8  | Bit 4: 1 = a new second since the last report has been started              |
|              |      | Bit 1: 1 = at least one DCF77 bit is lost                                   |
|              |      | Bit 0: 1 = DCF77 bit value is valid                                         |
|     bit      |   8  | last bit from the DCF77 bit stream if flags bit 0 is 1                      |
|              |      | 0: 0 bit received                                                           |
|              |      | 1: 1 bit received                                                           |
|              |      | 8: SYNC event received                                                      |
|              |      | 15: invalid bit received                                                    |
| stamp_offset |  16  | Count offset the last reference second has been started                     |
|  cur_phase   |  16  | This is the current phase between the reference second and the DCF77 signal |
| cur_interval |  16  | This is the current value of timer counts for one second                    |

Bit 4 in flags is used to let the host detect the begin of a new second even if
this event can't currently handled by the clock due to preocessing the current
USB query. For this case the host can detect from stamp_offset and cur_interval
the begin of the new second already happened.

How to interpret the data::

    ---------------------------------------------- time flow -------------------------------------------->

              |<----------- cur_interval (equal to one second) ----------------->|
    ref ------|------------------------------------------------------------------|-------------------------
    dcf ------------------------------------|--------------------------------------------------------------
                                            |---------- cur_phase -------------->|--- stamp_offset ---->|
                                                                                                        ^
                                                    this is the point of time we got in the report _____|

- ref = Events of the internal reference second
- dcf = Events of the DCF77 signal

The correct time is cur_phase + stamp_offset, because the internal reference time
follows the DCF signal with a cur_phase delay.

How to determine the point of time, reported via the stamp_offset value::

    USB  _____________________XXXXXXXXXXXXXXX_XX_________________________
    read _______________________XX_______________________________________
                              |<-------------->| ~1.08ms
                            ->|-|<-150us...250us
                              ->|-|<- ~150us to generate the answer

The whole transmission on the USB lines is happened in about 1ms. About
150µs...250µs after the transmission has started, the clock start to generate
the answer. E.g. stamp_offset defines this point of time. The next 150µs the
firmware needs to generate the whole answer. The remaining time is consumed by
the transmission routines.

And at the host?
----------------

One time stamp prior the USB query, a second time stamp when the data arrives
and the assumption the stamp_offset is equivalent to the middle between these
time stamps results into a good time. On my system the NTPD reports a jitter of
a few hundreds microseconds. For me it satisfies my requirements.

Building the firmware
---------------------

To compile the firmware you will need a GNU AVR cross compiler. I used the
GCC-4.3.2 in conjunction with the AVR C library 1.6.2 and the binutils 2.19.
Furthermore you will need the V-USB sources, at least the 20081126 revision.

Update
------

This project is from 2008, the V-USB project is still available, but I guess it
must be adapted to if a more recent version should be used with this firmeware.
On the other hand, this project might be just an idea how to implement it with
a real USB-aware microcontroller.

Happy hacking!
