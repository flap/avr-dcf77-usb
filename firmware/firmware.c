/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
   @file  firmware.c
   @brief Here is the startpoint for the executable of this firmware
*/

#include <avr/io.h>
#include <stdio.h>
#define __ASSERT_USE_STDERR
#include <assert.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <usbdrv.h>

#include "firmware.h"

int main(int argc, char *argv[]) __attribute__((naked));

#if 0
/**
 * We need some assembler to sleep race free
 * @param[in] control Pointer to a variable that must be zero to sleep
 */
static inline void sleep_well(uint8_t *control)
{
/* we must do here:
 * - disable interrupts
 * - sleep if there are no bits set in *control
 * - enable interrupts
 */
	sleep_mode();	/* FIXME THIS IS RACY! */
	/* TODO: Create some inline assembler */
}
#endif

#ifdef DEBUG
static char ver[] = "\nThe usb clock is entering the main loop (" __DATE__ "/" __TIME__ ")\n";
#endif

/**
 * main - here we start...
 * @param[in] argc argument count (not used)
 * @param[in] argv argument pointer vector (not used)
 * @return Should never return!
 */
int main(int argc, char *argv[])
{
	init_io_ports();	/* setup a save system state */
	init_uart();		/* on demand only */
	init_dcf77_trigger();

	set_sleep_mode(SLEEP_MODE_IDLE);
	usbInit();
	sei();

	/* give a hint, who we are */
	debugOut(ver);

	while(1) {
		usbPoll();
		handle_events();
#ifdef CALIBRATE
		print_calculated_values();
#endif
// 		sleep_well(&dcf77_status);
	}
}

/* end of file firmware.c */
