/***************************************************************************
 *   Copyright (C) 2008 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @file avr_dcf77.c
 * @brief Main routines to receive the clock via DFC77 and forward it via USB.
 */

#include <inttypes.h>
#include <avr/io.h>
#include <stdio.h>
#define __ASSERT_USE_STDERR
#include <assert.h>
#include <errno.h>

#include "firmware.h"

/* can be DATE_UNKNOWN, DATE_IN_SYNC_1, DATE_IN_SYNC_2, DATE_KEEP_AN_EYE, DATE_KNOWN */
static uint8_t date_reliability;
/* can be TIME_UNKNOWN, TIME_IN_SYNC_1, TIME_IN_SYNC_2, TIME_KEEP_AN_EYE, TIME_KNOWN */
static uint8_t time_reliability;

/*
 * we do our own local time counting here. The DCF77 signal will only
 * provide the correct time if it receive it correctly.
 * It will also provide our local clock with leap second, leap year and
 * other date information.
 *
 * Advancing the seconds, minutes and hour will be happen in interupt, because
 * this manipulation must be atomic. The interrupt routine will mark the
 * date invalid if also a date update must occure.
 *
 * Date updating cannot be handled in interrupt, because it takes too much time
 * and violates our USB timing requirements.
 */
uint8_t current_second;	/* handled atomic */
uint8_t current_minute;	/* handled atomic */
uint8_t current_hour;	/* handled atomic */

uint8_t current_day;
uint8_t current_month;
uint8_t current_year;

uint8_t date_needs_update;

/* global data */
uint16_t start_timestamp;
uint8_t sync_status;
uint8_t second;
uint8_t minute;
uint8_t hour;
uint8_t tz;
uint8_t day;
uint8_t month;
uint8_t year;

static int8_t next_minute;
static int8_t next_hour;
static int8_t next_tz;
static int8_t next_day;
static int8_t next_month;
static int8_t next_year;

void advance_day(void)
{
	if (!date_needs_update)
		return;
	/*
	 * We do not want to handle all leap years here in the code.
	 * This information is part of the DCF77 signal. The only restriction
	 * is, if the signal is bad. In this case we wait for better receiption.
	 * Until the date will be still marked as invalid.
	 */
	if (date_reliability == DATE_KEEP_AN_EYE || date_reliability == DATE_KNOWN) {
		current_day = next_day;
		current_month = next_month;
		current_year = next_year;
		date_needs_update = 0;
		sync_status |= DATE_SYNCED;	/* we are in sync again */
	}
}


/*
 * DCF77 encoding (from http://www.ptb.de/de/org/4/44/442/dcf_kode.htm)
 *
 * - bit 0: always 0
 * ---------------------------------------------------
 * - bit 1...14 other usage
 * ---------------------------------------------------
 * - bit 15: call (failure in the transmitter)
 * ---------------------------------------------------
 * - bit 16: 1=at the end of this hour CET/CEST will be switched
 * - bit 17: 0=CET, 1=CEST
 * - bit 18: 0=CEST, 1=CET
 * - bit 19: 1=at the end of this hour a leap second will be added
 * ---------------------------------------------------
 * - bit 20: always 1 (start of time info)
 * ---------------------------------------------------
 * - bit 21: 1=1 minute
 * - bit 22: 1=2 minutes
 * - bit 23: 1=4 minutes
 * - bit 24: 1=8 minutes
 * - bit 25: 1=10 minutes
 * - bit 26: 1=20 minutes
 * - bit 27: 1=40 minutes
 * - bit 28: minutes parity (even)
 * ---------------------------------------------------
 * - bit 29: 1=1 hour
 * - bit 30: 1=2 hour
 * - bit 31: 1=4 hour
 * - bit 32: 1=8 hour
 * - bit 33: 1=10 hour
 * - bit 34: 1=20 hour
 * - bit 35: hour parity (even)
 * ---------------------------------------------------
 * - bit 36: 1=1 day of month
 * - bit 37: 1=2 day of month
 * - bit 38: 1=4 day of month
 * - bit 39: 1=8 day of month
 * - bit 40: 1=10 day of month
 * - bit 41: 1=20 day of month
 * ---------------------------------------------------
 * - bit 42: 1=1 day of week
 * - bit 43: 1=2 day of week (1=monday, 7=sunday)
 * - bit 44: 1=4 day of week
 * ---------------------------------------------------
 * - bit 45: 1=1 month
 * - bit 46: 1=2 month
 * - bit 47: 1=4 month
 * - bit 48: 1=8 month
 * - bit 49: 1=10 month
 * ---------------------------------------------------
 * - bit 50: 1=1 year
 * - bit 51: 1=2 year
 * - bit 52: 1=4 year
 * - bit 53: 1=8 year
 * - bit 54: 1=10 year
 * - bit 55: 1=20 year
 * - bit 56: 1=40 year
 * - bit 57: 1=80 year
 * - bit 58: date parity (even -> 22 bits, from 36 to 58)
 * ---------------------------------------------------
 * - bit 59: not send, sync signal (or 0 in case of a leap second)
 *
 * In the case of a leap second:
 * - bit 59 is sent as a regular 0
 * - bit 60 will be the sync signal in this (rare) case
 *
 * Prior any leap second, bit 19 will be sent as 1 the whole hour before the
 * leap second. Otherwise bit 19 is always 0.
 * Prior switching between CET and CEST bit 16 will be sent as 1 the whole
 * hour before the switch happens.
 */

static uint8_t bit_buffer[31];
static uint8_t bit_pointer;

static uint8_t time_to_move;

/**
 * Stores one bit into the stream buffer
 * @param[in] value encoded bit value
 */
static void store_next_bit(uint8_t value)
{
	int index = bit_pointer >> 1;

	if (bit_pointer & 0x01) {
		bit_buffer[index] &= 0x0F;
		bit_buffer[index] |= value << 4;
	} else {
		bit_buffer[index] &= 0xF0;
		bit_buffer[index] |= value;
	}

	bit_pointer++;
	if (bit_pointer > 61)
		bit_pointer = 61;
}

static uint8_t read_bit(uint8_t number)
{
	int index = number >> 1, rc;

	assert(number < 62);

	if (number & 0x01)
		rc = bit_buffer[index] >> 4;
	else
		rc = bit_buffer[index] & 0x0F;

	return rc;
}

/**
 * @param[in] positions FIXME
 * @param[in] count FIXME
 * @return 0: Checksum okay, 1: checksum failure, 2: unable to calculate checksum due to bit failures
 */
static uint8_t checksum(const uint8_t positions, const uint8_t count)
{
	uint8_t u, onec = 0, rc;

	for (u = positions; u < (positions + count); u++) {
		rc = read_bit(u);
		switch(rc) {
			case BIT_UNKNOWN:
				debugOut("From %u, %u bits: Contains unknown bit!\n", positions, count);
				return 2;
			case BIT_1:
				onec++;
		}
	}
	/* parity is even */
	if (onec & 0x01){
		debugOut("Wrong parity for bits from %u, %u bits.\n", positions, count);
		return 1;
	}

	return 0;
}

/**
 * Check if all bits are valid
 * @param[in] positions FIXME
 * @param[in] count FIXME
 * @return 0: okay, 1: unable to calculate checksum due to bit failures
 */
static uint8_t check(const uint8_t positions, const uint8_t count)
{
	uint8_t u;

	for (u = positions; u < (positions + count); u++) {
		if (read_bit(u) == BIT_UNKNOWN)
			return 1;
	}

	return 0;
}

/**
 * Converts BCD notation into simple binary format
 * @param[in] positions FIXME
 * @param[in] count FIXME
 * @return decoded value
 */
static uint8_t decode_bcd(uint8_t position, uint8_t count)
{
	static const uint8_t value[8]={1,2,4,8,10,20,40,80};
	uint8_t ret = 0, i = 0;

	while (count) {
		if (read_bit(position) == BIT_1)
			ret += value[i];
		count--;
		position++;
		i++;
	}

	return ret;
}


/**
 * Extract he time zone for the next minute
 * @return 1 for CEST, 2 for CET
 */
static int8_t check_timezone(void)
{
	uint8_t lc;

	lc = decode_bcd(17,2);
	if ((lc == 0) || (lc == 3))
		return -1;

	return lc;
}

/**
 * @return -1 in case of failure, else next minute
 */
static int8_t check_minute(void)
{
	if (checksum(21, 8) == 0)
		return decode_bcd(21, 7);

	return -1;
}

/**
 * @return -1 in case of failure, else next hour
 */
static int8_t check_hour(void)
{
	if (checksum(29, 7) == 0)
		return decode_bcd(29, 6);

	return -1;
}

/**
 * @return -1 in case of failure, else next day of month
 */
static int8_t check_day_of_month(void)
{
	if (check(36, 6) == 0)
		return decode_bcd(36, 6);

	return -1;
}

static int check_month(void)
{
	if (check(45, 5) == 0)
		return decode_bcd(45, 5);

	return -1;
}

/**
 * Check the year in the next minute
 * @return -1 in case of failure, else next year
 *
 * TODO: Are 116 years enough? ;-)
 * (116 = 2127 - 2009)
 */
static int check_year(void)
{
	if (checksum(36, 23) == 0)
		return decode_bcd(50, 8);

	return -1;
}

/**
 * Handle the time component of the bit stream
 * @return 1 if the time overflows and must be accounted by the date
 */
static int8_t decode_time(void)
{
	int8_t overflow = 0;
	int8_t tmp;

	switch (time_reliability) {
	default:
	case TIME_UNKNOWN:
		sync_status &= ~TIME_SYNCED;
		next_minute = check_minute();
		if (next_minute == -1)
			break;
		next_hour = check_hour();
		if (next_hour == -1)
			break;
		time_reliability = TIME_IN_SYNC_1;
		break;

	case TIME_IN_SYNC_1:
		tmp = check_minute();
		if (tmp == -1 || tmp != (next_minute + 1)) {
			time_reliability = TIME_UNKNOWN;
			break;
		}
		next_minute = tmp;

		tmp = check_hour();
		if (tmp == -1 || tmp != next_hour) {
			time_reliability = TIME_UNKNOWN;
			break;
		}
		time_reliability = TIME_IN_SYNC_2;
		break;

	case TIME_IN_SYNC_2:
		tmp = check_minute();
		if (tmp == -1 || tmp != (next_minute + 1)) {
			time_reliability = TIME_IN_SYNC_1;
			break;
		}
		next_minute = tmp;

		tmp = check_hour();
		if (tmp == -1 || tmp != next_hour) {
			time_reliability = TIME_IN_SYNC_1;
			break;
		}
		time_reliability = TIME_KNOWN;
		break;

	case TIME_KEEP_AN_EYE:
		next_minute = check_minute();
		if (next_minute == -1) {
			/* try to repair the minute */
			next_minute = minute + 1;
			if (next_minute > 59)
				next_minute = 0;
			time_reliability = TIME_IN_SYNC_2;
			sync_status &= ~TIME_SYNCED;
		}

		if (next_minute < minute)
			overflow = 1;

		next_hour = check_hour();
		if (next_hour == -1) {
			/* try to repair the hour */
			next_hour = hour + overflow;
			if (next_hour > 23)
				next_hour = 0;
			time_reliability = TIME_IN_SYNC_2;
			sync_status &= ~TIME_SYNCED;
		}

		if (next_hour < hour)
			overflow = 1;
		else
			overflow = 0;
		if (time_reliability == TIME_KEEP_AN_EYE) {
				time_reliability = TIME_KNOWN;
		}
		break;

	case TIME_KNOWN:
		next_minute = check_minute();
		if (next_minute == -1) {
			/* try to repair the minute */
			next_minute = minute + 1;
			if (next_minute > 59)
				next_minute = 0;
			time_reliability = TIME_KEEP_AN_EYE;
		}

		if (next_minute < minute)
			overflow = 1;

		next_hour = check_hour();
		if (next_hour == -1) {
			/* try to repair the hour */
			next_hour = hour + overflow;
			if (next_hour > 23)
				next_hour = 0;
			time_reliability = TIME_KEEP_AN_EYE;
		}

		if (next_hour < hour)
			overflow = 1;
		else
			overflow = 0;

		if (time_reliability == TIME_KNOWN) {
			sync_status |= TIME_SYNCED;
		}
		break;
	}

	return overflow;
}

/**
 * Handle the date component of the bit stream
 * @param[in] overflow 1 if the time of day overflows
 *
 * FIXME: Date uses one parity bit for all parts (32 bits from bit 36 on)
 */
static void decode_date(int8_t overflow)
{
	int8_t tmp;

	switch (date_reliability) {
	default:
	case DATE_UNKNOWN:
		sync_status &= ~DATE_SYNCED;
		/*
		 * We know nothing about the date. We need at least
		 * one complete date set to do further tests.
		 */
		next_day = check_day_of_month();
		if (next_day < 0)
			break;
		next_month = check_month();
		if (next_month < 0)
			break;
		next_year = check_year();
		if (next_year < 0)
			break;
		/* one full set received */
		date_reliability = DATE_IN_SYNC_1;
		break;

	case DATE_IN_SYNC_1:
		/*
		 * at least one date set we already received. We are
		 * waiting for a second one, to check the information.
		 * This will fail, when someone starts near midnight...
		 * At this point of time it will take more time to sync in.
		 */
		tmp = check_day_of_month();
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != next_day) {
			date_reliability = DATE_UNKNOWN; /* start again */
			break;
		}

		tmp = check_month();
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != next_month) {
			date_reliability = DATE_UNKNOWN; /* start again */
			break;
		}

		tmp = check_year();
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != next_year) {
			date_reliability = DATE_UNKNOWN; /* start again */
			break;
		}
		date_reliability = DATE_IN_SYNC_2;
		break;

	case DATE_IN_SYNC_2:
		/*
		 * at least two date sets we already received. We are
		 * waiting for a third one, to check the information.
		 * This will fail, when someone starts near midnight...
		 * But it only will take more time to sync in.
		 */
		tmp = check_day_of_month();
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != next_day) {
			date_reliability = DATE_IN_SYNC_1; /* fall back */
			break;
		}

		tmp = check_month();
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != next_month) {
			date_reliability = DATE_IN_SYNC_1; /* fall back */
			break;
		}

		tmp = check_year();
		if (tmp < 0)
			break;	/* ignore */
		if (tmp != next_year) {
			date_reliability = DATE_IN_SYNC_1; /* fall back */
			break;
		}
		date_reliability = DATE_KNOWN;
		break;

	case DATE_KEEP_AN_EYE:
		/*
		 * We are synced and trust our stored date. But the new data
		 * seems instable, so we must keep an eye on it, if we can
		 * still trust on it. If the new data again is faulty, we
		 * fall back to an earlier check state.
		 */
		next_day = check_day_of_month();
		if (next_day == -1) {
			date_reliability = DATE_IN_SYNC_1; /* fall back */
			sync_status &= ~DATE_SYNCED;
		}

		next_month = check_month();
		if (next_month == -1) {
			date_reliability = DATE_IN_SYNC_1; /* fall back */
			sync_status &= ~DATE_SYNCED;
		}

		next_year = check_year();
		if (next_year == -1) {
			date_reliability = DATE_IN_SYNC_1; /* fall back */
			sync_status &= ~DATE_SYNCED;
		}
		/* Seems okay again now. Back to known state */
		if (date_reliability == DATE_KEEP_AN_EYE)
			date_reliability = DATE_KNOWN;

		next_tz = check_timezone();
		break;

	case DATE_KNOWN:
		/*
		 * It seems we know the current date. Start to
		 * trust the information. And fix it in case of failures.
		 */
		next_day = check_day_of_month();
		if (next_day == -1) {
			/* try to repair the day of month */
			date_reliability = DATE_KEEP_AN_EYE;
			if (overflow != 0) {
				if (day < 28)
					next_day = day + 1;
				else {
					/* dilemma: How to know when this day of month overflows? */
					/* we need the current month (and year) to decide! */
					sync_status &= ~DATE_SYNCED;
				}
			} else
				next_day = day;
		}

		if (next_day < day)
			overflow = 1;
		else
			overflow = 0;

		next_month = check_month();
		if (next_month == -1) {
			date_reliability = DATE_KEEP_AN_EYE;
			/* try to repair the month */
			next_month = month + overflow;
			if (next_month > 12)
				next_month = 12;
		}

		if (next_month < month)
			overflow = 1;
		else
			overflow = 0;

		next_year = check_year();
		if (next_year == -1) {
			date_reliability = DATE_KEEP_AN_EYE;
			/* try to repair the year */
			next_year = year + overflow;	/* BUG: if year = 127 and overflow = 1! */
		}

		if (date_reliability == DATE_KNOWN) {
			sync_status |= DATE_SYNCED;
			next_tz = check_timezone();
		}
		break;
	}
}

/**
 * marks data set as invalid
 * This happens whenever something went wrong with the receiver
 * @param[inout] buffer where to mark
 * @param[in] count how many bytes to mark
 **/
void mark_dataset_invalid(void)
{
	unsigned i;

	for (i = 0; i < sizeof(bit_buffer); i++)
		bit_buffer[i] = BIT_UNKNOWN | (BIT_UNKNOWN << 4);
}

static void decode_stream(void)
{
	int8_t tmp;

	tmp = decode_time();
	decode_date(tmp);
}


/**
 * Sort in the next bit
 * @param[in] value Code of the next bit
 */
void report_bit(uint8_t value)
{
	switch(value) {
	case BIT_UNKNOWN:	/* garbage from DCF77 receiver */
#ifdef DEBUG
		UDR = 'X';
#endif
		store_next_bit(BIT_UNKNOWN);
		break;

	case BIT_SYNC:	/* no signal from DCF77 receiver */
#ifdef DEBUG
		UDR = 'S';
#endif
		if (bit_pointer < 59)
			bit_pointer = 0;
		else {
			decode_stream();
			time_to_move = 1;
			bit_pointer = 0;
		}
		UDR = '\r';
		UDR = '\n';
		break;

	case BIT_1:	/* one bit from DCF77 receiver */
#ifdef DEBUG
		UDR = '1';
#endif
		store_next_bit(BIT_1);
		break;

	case BIT_0:	/* zero bit from DCF77 receiver */
#ifdef DEBUG
		UDR = '0';
#endif
		store_next_bit(BIT_0);
		break;

	default:
#ifdef DEBUG
		UDR = '?';
#endif
		break;
	}
}

/**
 * Report the start of the next second
 */
void report_second(void)
{
	if (time_to_move == 1) {
		time_to_move = 0;
		/* begin of atomic operation */
		second = 0;
		tz = next_tz;
		minute = next_minute;
		hour = next_hour;
		day = next_day;
		month = next_month;
		year = next_year;
		start_timestamp = second_start_time_stamp;
		/* end of atomic operation */
	} else {
		/* begin of atomic operation */
		second ++;
		start_timestamp = second_start_time_stamp;
		/* end of atomic operation */
	}
}

