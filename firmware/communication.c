/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <avr/io.h>
#define __ASSERT_USE_STDERR
#include <assert.h>
#include <avr/pgmspace.h>
#include <usbdrv.h>

#include "firmware.h"

/**
 * @file communication.c
 * @brief Report some received information to the host
 */

/*
 * bit 0: 1=valid bit to be reported
 * bit 1: 1=one bit missed
 * bit 2...7: 0=reserved
 */
static volatile uint8_t valid;
static uint8_t bit_value;

struct communication {
	uint8_t flags;		/* unit digits, min 0, max 0xff -> dynamic value 8 bit? */
	uint8_t bit;		/* unit digits, min 0, max 0xff */
	uint16_t stamp_offset;	/* unit digits, min 0, max 0xffff */
	uint16_t cur_phase;	/* unit digits, min 0, max 0xffff */
	uint16_t cur_interval;	/* unit digits, min 0, max 0xffff */
};

#define DATA_SIZE sizeof(struct communication)

/**
 * Timing behaviour of the communication:
 *
 * USB  _____________________XXXXXXXXXXXXXXX_XX_________________________ (T6)
 * read _______________________XX_______________________________________ (T4)
 *                           |<-------------->| ~1.08ms
 *                          >|-|< 150us...250us
 *                            >|-|< ~150us to generate the answer
 *
 * The whole read of one USB package needs about 1.08ms and 21 interrupts.
 * After about 150us...250us the USB driver calls the usbFunctionSetup() and
 * about about 150us later it returns with the anwser.
 *
 * If another USB activity is happen it will also generate such a timing.
 * T4 shows a small spike in this case. (happen here with my host, due to
 * running 'doperterm' at the same time. MiniDoper, also a low speed device).
 *
 * FIXME: Why is usbFunctionSetup called when the host addresses another
 * target on the bus? Bug in the driver?
 */

static struct communication comm_buffer;

/**
 * Feed in the next received DCF bit
 * @param[in] next_bit the next bit to report
 *
 * @todo Called by the main loop. So, interrupts should be enabled?
 */
void report_bit(const uint8_t next_bit)
{
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		bit_value = next_bit;
		valid <<= 1;
		valid |= 0x01;
		valid &= 0x03;
	}
}

/* ######### the following part is the connector to the usblib ######## */

/* USB report descriptor
 * TODO: Try to understand what it means...
 */
PROGMEM char usbHidReportDescriptor[22] = {
	0x06,	/* Generic Device Controls Page, chapter 9, hut1_12.pdf */
	0x00, 0xff,	/* USAGE_PAGE (Vendor Defined) */
	0x09, 0x01,		/* USAGE (I/O Device) */
	0xa1, 0x01,		/* COLLECTION (Application) */
	0x15, 0x00,		/*   LOGICAL_MINIMUM (0) */
	0x26, 0xff, 0x00,	/*   LOGICAL_MAXIMUM (255) */
	0x75, DATA_SIZE,	/*   REPORT_SIZE */
	0x95, 0x08,		/*   REPORT_COUNT (128) */ /* FIXME Nicht eher 8? */
	0x09, 0x00,		/*   USAGE (Undefined) */
	0xb2, 0x02, 0x01,	/*   FEATURE (Data,Var,Abs,Buf) */
	0xc0			/* END_COLLECTION */
};

/*
 * F�r meine Struktur:
 *
 *             UsagePage(Vendor defined),
 *             Usage(I/O device),
 *             Collection(Application),
 *
 *               Usage(flags),			; the communication.flags element
 *               Collection(Logical),
 *                 Usage(valid),		; bit 0 in communication.flags
 *                 Usage(lost),			; bit 1 in communication.flags
 *                 ReportSize(1),		; one bit each
 *                 ReportCount(2),		; two bits
 *                 Feature(Constant, Variable, Absolute),		; ??????
 *                 ReportSize(1),		; one bit each
 *                 ReportCount(6),		; 6 bits unused in communication.flags
 *                 Feature(Const),
 *               End Collection(),
 *
 *               Usage(bit),			; the communication.bit element
 *               Collection(Logical),
 *                 Usage(event),		; bit 0..3 in communication.bit
 *                 ReportSize(4),		; 4 bit
 *                 ReportCount(1),		; one element only
 *                 Logical Minimum(0),
 *                 Logical Maximum(15),
 *                 Feature(Constant, Variable, Absolute),		; ??????
 *                 ReportSize(4),		; 4 bit
 *                 ReportCount(1),		; 4 bits unused in communication.bit
 *                 Feature(Const),
 *               End Collection(),
 *
 *               Usage(offset),			; communication.stamp_offset
 *               Usage(phase),			; communication.cur_phase
 *               Logical Minimum(0),
 *               Logical Maximum(65535),
 *               ReportSize(16),		; 16 bit each
 *               ReportCount(2),		; two elements
 *               Feature(Constant, Variable, Relative),	; ?????
 *
 *               Usage(internval),		; communication.cur_interval
 *               Logical Minimum(0),
 *               Logical Maximum(65535),
 *               ReportSize(16),		; 16 bit each
 *               ReportCount(1),		; one element only
 *               Feature(Constant, Variable, Absolute),	; ?????
 *
 *            End Collection(),
 */

/*
          Report Descriptor: (length is 22)
            Item(Global): Usage Page, data= [ 0x00 0xff ] 65280
                            (null)
            Item(Local ): Usage, data= [ 0x01 ] 1
                            (null)
            Item(Main  ): Collection, data= [ 0x01 ] 1
                            Application
            Item(Global): Logical Minimum, data= [ 0x00 ] 0
            Item(Global): Logical Maximum, data= [ 0xff 0x00 ] 255
            Item(Global): Report Size, data= [ 0x08 ] 8
            Item(Global): Report Count, data= [ 0x80 ] 128
            Item(Local ): Usage, data= [ 0x00 ] 0
                            (null)
            Item(Main  ): Feature, data= [ 0x02 0x01 ] 258
                            Data Variable Absolute No_Wrap Linear
                            Preferred_State No_Null_Position Non_Volatile Buffered Bytes
            Item(Main  ): End Collection, data=none
*/

#if 0
/* another example for an 8 bit in and 8 bit out device */
const byte ReportDescriptor[] = {
        0x06, 0x00, 0xff,	// Usage_Page (Vendor Defined)
        0x09, 0x01,		// Usage (I/O Device)
        0xA1, 1,		// Collection (Application)
        0x19, 1,       //   Usage_Minimum (Button 1)
        0x29, 8,       //   Usage_Maximum (Button 8)
        0x15, 0,       //   Logical_Minimum (0)
        0x25, 1,       //   Logical_Maximum (1)
        0x75, 1,       //   Report_Size (1)
        0x95, 8,       //   Report_Count (8)
        0x81, 2,       //   Input (Data,Var,Abs) = Buttons Value
        0x19, 1,       //   Usage_Minimum (LED 1)
        0x29, 8,       //   Usage_Maximum (LED 8)
        0x91, 2,       //   Output (Data,Var,Abs) = LEDs (5 bits)
        0xC0           // End_Collection
        };

#endif

/**
 * called by usbProcessRx() for setup token
 * @param[in] data the received data
 * @return 0 or USB_NO_MSG
 *
 * This function is called when the driver receives a SETUP transaction from
 * the host which is not answered by the driver itself (in practice: class and
 * vendor requests). All control transfers start with a SETUP transaction where
 * the host communicates the parameters of the following (optional) data
 * transfer. The SETUP data is available in the 'data' parameter which can
 * (and should) be casted to 'usbRequest_t *' for a more user-friendly access
 * to parameters.
 *
 * If the SETUP indicates a control-in transfer, you should provide the
 * requested data to the driver. There are two ways to transfer this data:
 * (1) Set the global pointer 'usbMsgPtr' to the base of the static RAM data
 * block and return the length of the data in 'usbFunctionSetup()'. The driver
 * will handle the rest. Or (2) return USB_NO_MSG in 'usbFunctionSetup()'. The
 * driver will then call 'usbFunctionRead()' when data is needed. See the
 * documentation for usbFunctionRead() for details.
 *
 * If the SETUP indicates a control-out transfer, the only way to receive the
 * data from the host is through the 'usbFunctionWrite()' call. If you
 * implement this function, you must return USB_NO_MSG in 'usbFunctionSetup()'
 * to indicate that 'usbFunctionWrite()' should be used. See the documentation
 * of this function for more information. If you just want to ignore the data
 * sent by the host, return 0 in 'usbFunctionSetup()'.
 *
 * Note that calls to the functions usbFunctionRead() and usbFunctionWrite()
 * are only done if enabled by the configuration in usbconfig.h.
 */

/*
typedef struct usbRequest{
    uchar       bmRequestType;
    uchar       bRequest;
    usbWord_t   wValue;
    usbWord_t   wIndex;
    usbWord_t   wLength;
}usbRequest_t;
*/

usbMsgLen_t usbFunctionSetup(uint8_t data[8])
{
	struct usbRequest *rq = (usbRequest_t*)data;

	set_usb_read();

	if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) {    /* HID class request */
		/*
		 * Possible calls here are:
		 * 0x01:   GET_REPORT
		 * 0x02:   GET_IDLE
		 * 0x03:   GET_PROTOCOL
		 * 0x04-0x08:    Reserved
		 * 0x09:   SET_REPORT
		 * 0x0A:   SET_IDLE
		 * 0x0B:   SET_PROTOCOL
		 *
		 * Can we savely ignore the others?
		 */
		if (rq->bRequest == USBRQ_HID_GET_REPORT) {  /* wValue: ReportType (highbyte), ReportID (lowbyte) */
			uint16_t tmp1, tmp2;
			uint8_t tmp3;

 			DEBUGC('r');
		/*
		 * NOTE: We must ensure here all values are read inside the atomic block.
		 * There should be no reordering of instructions happen. Classifying the temp vars
		 * as 'volatile' creates very bad code with the 4.3.2 AVR compiler. Without
		 * 'volatile ' code is much better and with this version of the compiler everything
		 * important still happens inside the atomic block (=expected behaviour).
		 */
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
				tmp1 = second_start_time_stamp;
				tmp2 = TCNT1;
				tmp3 = timer_status;
				timer_status &= ~NEW_SECOND_STARTS;
			}

			comm_buffer.stamp_offset = calc_count_diff(tmp1, tmp2);
			comm_buffer.flags = valid;
			if (tmp3 & NEW_SECOND_STARTS)
				comm_buffer.flags |= 0x10;
			comm_buffer.bit = bit_value;
			comm_buffer.cur_phase = get_average_phase_value();
			comm_buffer.cur_interval = get_average_interval_value();
			valid = 0;

			usbMsgPtr = &comm_buffer;
			reset_usb_read();
			return sizeof(comm_buffer);
		}
	}

	reset_usb_read();
	return 0;
}
