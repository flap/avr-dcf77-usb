/***************************************************************************
 *   Copyright (C) 2008 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @file avr_dcf77.h
 **/

/** Crystal speed can be setup from extern */
#define CRYSTAL (F_CPU)

#define BAUDRATE (9600)
#define UBRR_REG ((CRYSTAL / (16 * BAUDRATE))-1)

/*
 * PRE_SCALER should scale down the crystal's clock into
 * a limit up to 65535!
 * For 16MHz crystal a prescaler of 256 is ok
 */
#define PRE_SCALER (0x4)
/* #define TIMER_TOP (62500) */
#define TIMER_TOP (63600)

#define AVERAGE_COUNT 10

extern uint16_t calibrate_interal_clocks(void);

extern void set_status_led(void);
extern void clear_status_led(void);
extern void invert_status_led(void);
extern void set_pulse_led(void);
extern void clear_pulse_led(void);
extern void invert_pulse_led(void);

#define PULSE_LED PD2
#define STATUS_LED PD0

/* end of file avr_dcf77.h */
