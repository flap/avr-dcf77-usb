/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <avr/interrupt.h>

#ifdef DEBUG
# define debugOut(x,y...) printf(x,## y)
# define DEBUGC(x) UDR=x
# define CALIBRATE
# define PRESENT_TIMING
#else
# define debugOut(x,y...)
# define DEBUGC(x)
# undef CALIBRATE
# undef PRESENT_TIMING
#endif

#ifndef ARRAY_SIZE
# define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#endif

#ifndef __ASSEMBLER__
extern volatile uint8_t timer_status;
#endif

/* meaning of bits in 'timer_status': */

/* internal timer expires */
#define TIMER_REFERENCE 1

/* we received the next start of second event */
#define NEXT_SECOND_STARTS 2

/* dcf77 bit complete */
#define NEXT_BIT_ARRIVED 4

/* some garbage on the line */
#define NEW_BIT_IS_GARBAGE 8

/* no DCF77 edge within a second */
#define MISSED_DCF_EDGE 16

/* for the USB interface: atomicaly set bit to mark the begin of a new second */
#define NEW_SECOND_STARTS 128

/* counts timer 1 advances in one second -> 21.333us resolution */
#define COUNT_PER_SECOND 46875

#define BIT_UNKNOWN 15
#define BIT_SYNC 8
#define BIT_1 1
#define BIT_0 0

/*
 * The currently used DCF77 receiver shows:
 * - 80ms for the 0 bit
 * - 190ms for a 1 bit
 */
/* below 75ms is too short */
#define PULSE_TOO_SMALL 3516
/* 300ms is too long */
#define PULSE_TOO_WIDE 14062

/* 150ms distinguish between short (100ms) and long (200ms) */
#define PULSE_SHORT 7031

/* define a too short second interval to sort it out */
#define INTERVAL_TOO_SHORT (COUNT_PER_SECOND - 800)

/* define a too long second interval to sort it out */
#define INTERVAL_TOO_LONG (COUNT_PER_SECOND + 800)

/* count of samples to calculate the average interval */
#define INTERVAL_SUM_COUNT 300U

/* count of samples that are used all over the time */
#define INTERVAL_SUM_COUNT_MAX 65000U

/* count of samples to calculate the average phase difference (max 254!) */
#define PHASE_SUM_COUNT 10

/* phase the reference edge should be earler than the DCF77 edge */
#define PHASE_RESERVE 20000

#ifndef __ASSEMBLER__

#include <util/atomic.h>

/* from firmware.c */
extern volatile uint16_t second_start_time_stamp;
extern uint16_t calc_count_diff(const uint16_t, const uint16_t);

/* from internal.c */
extern void init_io_ports(void);
extern void init_uart(void);
extern void init_dcf77_trigger(void);

/* from event_handler.c */
extern void handle_events(void);
extern uint16_t get_average_phase_value(void);
extern uint16_t get_average_interval_value(void);
#ifdef CALIBRATE
void print_calculated_values(void);
#endif

/* from communication.c */
extern void report_bit(const uint8_t);

static inline void enable_yellow_led(void)
{
	PORTB &= ~0x04;
}

static inline void disable_yellow_led(void)
{
	PORTB |= 0x04;
}

static inline void invert_yellow_led(void)
{
	PORTB ^= 0x04;
}

static inline void enable_red_led(void)
{
	PORTB &= ~0x02;
}

static inline void disable_red_led(void)
{
	PORTB |= 0x02;
}

static inline void invert_red_led(void)
{
	PORTB ^= 0x02;
}


#if 0
/* no need for C code here, as it is done in assembler (refer usbdrvasm12.inc)
 * Can be measured at 'T6'
 */
static inline void set_usb_irq(void)
{
#ifdef PRESENT_TIMING
	PORTC |= 0x20;
#endif
}

static inline void reset_usb_irq(void)
{
#ifdef PRESENT_TIMING
	PORTC &= ~0x20;
#endif
}
#endif

static inline void set_usb_read(void)
{
#ifdef PRESENT_TIMING
	PORTC |= 0x08;
#endif
}

static inline void reset_usb_read(void)
{
#ifdef PRESENT_TIMING
	PORTC &= ~0x08;
#endif
}

#endif /* __ASSEMBLER__ */

/* what interrupt should show us the internal timer reference */
#define TIMER_REFERENCE_IRQ TIMER1_COMPB_vect
