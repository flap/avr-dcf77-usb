/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

/**
 * @file internal.c
 * @brief Low end hardware specific access routines
 */

#include <avr/io.h>
#include <stdio.h>

#include "firmware.h"

#ifdef DEBUG
/**
 * Send one char
 * @param[in] c char to send
 * @param[in] stream stream to send to
 */
static int uart_putchar(char c, FILE *stream)
{
	loop_until_bit_is_set( UCSRA, UDRE );
	UDR = c;
	return 0;
}

static FILE mystdout = FDEV_SETUP_STREAM( uart_putchar, NULL, _FDEV_SETUP_WRITE );
#endif

/**
 * configure the UART to send debug messages while development
 */
void init_uart(void)
{
#ifdef DEBUG

#define BAUD  19200L

#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD)

#if ((BAUD_ERROR < 999) || (BAUD_ERROR > 1010))
# error "Baudrate impossible at this main clock speed!"
#endif

	/* Enable the TxD pin */
	DDRD |= 0x02;

	UCSRB |= (1 << TXEN);

	UBRRH = (uint8_t)(UBRR_VAL >> 8);
	UBRRL = (uint8_t)(UBRR_VAL & 0xFF);

	UCSRC |= (1 << URSEL)|(1 << UCSZ1)|(1 << UCSZ0);

	/* redirect stdout to my routine */
	stdout = &mystdout;
	stderr = &mystdout;
#endif
}

/**
 * initIOPorts - setup a save system state to all io ports
 */
void init_io_ports(void)
{
/* Port B used as:
 Bit 7: XTAL2
 Bit 6: XTAL1
 Bit 5: ISP SCK (input with pullup enabled)
 Bit 4: ISP MISO (input with pullup enabled)
 Bit 3: ISP MOSI (input with pullup enabled)
 Bit 2: LED D7 (colour yellow) (low=on)
 Bit 1: LED D6 (colour red) (low=on)
 Bit 0: T2 free (input with pullup enabled)
*/
	PORTB = 0x3f;
	DDRB = 0x06;

/* Port C used as:
 Bit 7: unusable
 Bit 6: input, external reset (low active)
 Bit 5: T6 free (input with pullup enabled) -> if enabled: usb-irq-timing, then output, 1 entered, 0 left
 Bit 4: T8 free (input with pullup enabled) -> if enabled: , then output, 1 entered, 0 left
 Bit 3: T4 free (input with pullup enabled) -> if enabled: usb-read-timing, then output, 1 entered, 0 left
 Bit 2: JP1 (input with pullup enabled)
 Bit 1: T3 free (input with pullup enabled)
 Bit 0: T5 free (input with pullup enabled)
*/
#ifdef PRESENT_TIMING
	PORTC = 0xc7;
	DDRC = 0x38;
#else
	PORTC = 0x7f;
	DDRC = 0x00;
#endif

/* Port D used as:
 Bit 7: DCF77 signal (=AIN1)
 Bit 6: reference voltage for DCF77 input (=AIN0)
 Bit 5: T1 (input with pullup enabled)
 Bit 4: bidirectional, USB negative
 Bit 3: DCF77 interrupt (input with pullup enabled)
 Bit 2: bidirectional, USB positive
 Bit 1: output, TxD for serial console (input with pullup enabled if not used as TxD)
 Bit 0: input, RxD for serial console (input with pullup enabled)
*/
	PORTD = 0x2b;
	DDRD = 0x00;
}

/**
 * Setup the analouge comparator for timer capturing
 *
 * Whenever the DCF77 signal sends an edge, timer 1 should capture its current
 * timer value and the comparator should generate an interrupt. Also the
 * timer 1 should generate an interrupt, when there is no edge for 1 second.
 */
void init_dcf77_trigger(void)
{
	SFIOR &= ~0x08;	/* connect AIN1 to the negative input */
	ACSR = 0x14;

	TCCR1A = 0x00;
	TCCR1B = 0x00;	/* ICP1 noise canceler disabled, falling edge triggers */
	TCNT1 = 0x0000;
	OCR1A = COUNT_PER_SECOND + 2000;
	OCR1B = COUNT_PER_SECOND;

	TIFR = 0x3C;	/* clear all status bits */

	TIMSK = 0x08;	/* OCR1B interrupt is enabled */

/* timer runs at 12MHz / 256 = 46.875kHz. The 16 bit timer needs 1.398s to overrun */
	TCCR1B |= 0x04;	/* main clock / 256 */
}

/* end of file internal.c */
