/***************************************************************************
 *   Copyright (C) 2008 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @file calibrate_dcf.c
 * @brief FIXME
 **/

#include <avr/io.h>
#include <stdio.h>
#include "avr_dcf77.h"

#if 0
extern uint16_t frequency;

/**
 * calibrate internal clocks
 *
 * Step 1:
 * - Setup prescaler and start the 16 bit timer
 * - Use capture mode to find the counter value for one second
 * - Wait for the next falling edge on the DCF77 signal
 * - Reset 16 bit timer to zero and reset the prescaler
 * - Wait for the next falling edge on the DCF77 signal
 *
 * TODO: Use the capturing feature of the processor to get a more precise value
 **/

static uint16_t do_one_calibration(void)
{
uint16_t timer_max;

	clear_pulse_led();
	putchar('1');
	SFIOR |= _BV(PSR10);	/* reset prescaler */
	TCNT1H = 0x00;
	TCNT1L = 0x00;			/* keep timer at zero */
	TIFR |= _BV(TOV1);
/*
 * Loop until DCF77 signal is high
 */
	while (!(PIND & _BV(PIND3))) {
		SFIOR |= _BV(PSR10);	/* reset prescaler */
		TCNT1H = 0x00;
		TCNT1L = 0x00;			/* keep timer at zero */
	}
/*
 * Loop until a falling edge on the DCF77 signal
 * and keep timer at zero to get a counter value
 * as precise as possible
 */
	while (PIND & _BV(PIND3)) {
		SFIOR |= _BV(PSR10);	/* reset prescaler */
		TCNT1H = 0x00;
		TCNT1L = 0x00;			/* keep timer at zero */
	}
	set_pulse_led();
	putchar('2');
/*
 * now the timer is running.
 * Loop until DCF77 signal is high again
 */
	while (!(PIND & _BV(PIND3)))
		;
	putchar('3');
/*
 * loop until the next rising edge
 */
	while (PIND & _BV(PIND3))
		;
	if (TIFR & _BV(TOV1))	/* overflow? */
		return 0;	/* not valid */

	timer_max = ICR1L;
	timer_max |= ((uint16_t)ICR1H)<<8;
	putchar('4');
	putchar('\n');

	return (timer_max);
}

/**
 *
 **/
static uint16_t calculate_average(uint16_t *table,int count)
{
uint32_t average;
int i;

	for (i=0;i<count;i++) {
		if (table[i] != 0)
			break;
	}
	if (i >= count)
		return 0;	/* FIXME */

	average = table[i];
	printf("\n% 5u -> % 5lu\n",table[0],average);
	for (;i<count;i++) {
		if (table[i] != 0) {
			average += table[i];
			average >>= 1;
		}
		printf("% 5u -> % 5lu\n",table[i],average);
	}
	return (uint16_t)average;
}

/**
 * Note: Call this function with disabled interrupts!
 **/

uint16_t calibrate_interal_clocks(void)
{
uint16_t	result[AVERAGE_COUNT];
uint16_t	average;
int			i;
/*
 * run timer in normal mode
 */
	TCCR1A = 0x00;
	TCCR1B = 0x04;	/* capture at falling edge, prescaler /256 is enough for 16MHz */
	set_status_led();
#if 1
/*
 * calibration loop should run 20 times.
 * There is a chance of 1/60 to find the 2 second sync pause
 * and failing here with a correct timer value!
 */
	for (i=0;i<AVERAGE_COUNT;i++) {
		result[i]=do_one_calibration();	/* this needs about 40 Seconds! */
		invert_status_led();
	}
/*
 * calculate the average of this list to find a 2 second intervall
 * that's maybe is in this list
 */
	average=calculate_average(result,AVERAGE_COUNT);
	printf("->%u\n",average);
/*
 * If there is a 2 second intervall in the list, the average
 * is always larger than the 1 second intervall
 */
#if 0
	for (i=0;i<AVERAGE_COUNT;i++) {
		if (result[i] > average)
			result[i]=0;	/* invalid! 2 Seconds intervall? */
	}
/*
 * recalculate to get a usefull value
 */
	average=calculate_average(result,AVERAGE_COUNT);
#endif
/*
 * decide what values should be used to get correct
 * future measure results. Capture value will be
 * - ~62500 at 16MHz
 * - ~31250 at 8MHz
 * - ~15625 at 4MHz
 * - ~7812 at 2MHz
 * - ~3906 at 1MHz
 */
	if (average < 20000) {
		TCCR1B = (TCCR1B&0xF8) | 0x03;	/* capture at rising edge, prescaler /64 */
	/*
	 * redo calculation
	 */
		for (i=0;i<AVERAGE_COUNT;i++) {
			result[i]=do_one_calibration();	/* this needs about 40 Seconds! */
			invert_status_led();
		}
		average=calculate_average(result,AVERAGE_COUNT);
		for (i=0;i<AVERAGE_COUNT;i++) {
			if (result[i]>average)
				result[i]=0;	/* invalid! 2 Seconds intervall? */
		}
		average=calculate_average(result,AVERAGE_COUNT);
	}

	clear_status_led();
	printf("->%u\n",average);
#endif
	return TIMER_TOP;
	/* return average+1; */
}

/* end of file calibrate_dcf.c */

#endif
