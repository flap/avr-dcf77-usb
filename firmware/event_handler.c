/***************************************************************************
 *   Copyright (C) 2009 by Juergen Borleis                                 *
 *   projects@ohnezutaten.de                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
   @file  event_handler.c
   @brief Handling of all timer relevant events is done here
*/

#include <avr/io.h>
#include <stdio.h>
#define __ASSERT_USE_STDERR
#include <assert.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <usbdrv.h>

#include "firmware.h"

/**
 * Timer stamp of the current running second
 * Pay attention: Never modify or read this variable directly! Its modified
 * in an interrupt routine.
 * BTW: The interrupt routine is the reason this variable must be global.
 */
volatile uint16_t second_start_time_stamp;

/**
 * Current state machine's state
 * Pay attention: Never modify this variable directly! Its also modified
 * in an interrupt routine.
 * BTW: The interrupt routine is the reason this variable must be global.
 * Note: Keep it 8 bit, to ensure a simple read is atomic.
 */
volatile uint8_t timer_status;

struct access_optimized {
	uint16_t dcf77_start_pulse_stamp;
	uint16_t dcf77_old_start_pulse_stamp;
	uint16_t dcf77_stop_pulse_stamp;

/* derivation from COUNT_PER_SECOND */
	int16_t period_correction;

/* to sync internal start of second to the DCF77 start of second marker */
	int16_t phase_correction;

	uint16_t average_interval_count;
	uint32_t interval_sum;

	uint8_t average_phase_count;
	uint32_t average_phase_diff;

	uint8_t cycle_state;
};

/**
 * There is no real need for this structure than to give the compiler
 * an additional way to optimizing the code. This was a tip at one of the
 * website for Atmega programmers.
 *
 * - 3106 bytes separate vars
 * - 3106 bytes with structured vars (WOW!)
 */
static struct access_optimized var = {
	.interval_sum = 20 * COUNT_PER_SECOND,
	.average_interval_count = 20
};

#define DCF_EDGE_HAPPEND 1
#define REF_EDGE_HAPPEND 2
#define LAST_DCF_EDGE_HAPPEND 4
#define AWAITING_DCF_EDGE 8
#define DCF_TIMEOUT_ARMED 16

#ifdef CALIBRATE
static uint16_t current_phase_diff;
static int16_t current_phase_correction;
static uint8_t new_val;
static uint16_t new_period_value;
#endif

/**
 * Resetting some bits in the 'timer_status'
 * @param[in] value bits to reset
 * We must protect this manipulation against an interrupt routine
 */
static void reset_status_bits(const uint8_t value)
{
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		timer_status &= ~value;
	}
}

/**
 * Setting some bits in the 'timer_status'
 * @param[in] value bits to set
 * We must protect this manipulation against aninterrupt routine
 */
static void set_status_bits(const uint8_t value)
{
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		timer_status |= value;
	}
}

/**
 * Reading the second start time stamp must be an atomic operation
 * @return current value of this variable
 *
 * Note: The second_start_time_stamp variable will be modified in an
 * interrupt routine. Always read this variable through this function!
 */
static uint16_t read_second_start_time_stamp(void)
{
	uint16_t tmp;

	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		tmp = second_start_time_stamp;
	}

	return tmp;
}

/**
 * Read the Input Capture Register in a save manner
 * @return Current value from the ICR1
 *
 * Read this 16 bit register in an interrupt save manner
 */
static uint16_t read_dcf_edge_stamp(void)
{
	uint16_t tmp;
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		tmp = ICR1;
	}
	return tmp;
}

/**
 * Read the current DCF77 timeout detection time (=read the OCRA1 in a save manner)
 * @return Current value from the OCR1A
 *
 * Read this 16 bit register in an interrupt save manner
 */
static uint16_t read_timeout_detection(void)
{
	uint16_t tmp;
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		tmp = OCR1A;
	}
	return tmp;
}

/**
 * Setup a new DCF77 timeout detection time (=Write the OCRA1 in a save manner)
 * @param[in] val New value for the OCR1A
 *
 * Write this 16 bit register in an interrupt save manner
 */
static void set_timeout_detection(const uint16_t val)
{
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		OCR1A = val;
		TIFR = 0x10;	/* clear the OCF1A flag (*and* do not clear other flags! E.g. do not use |= here! */
	}
}

/**
 * Setup the next reference event (=write the OCRB1 in a save manner)
 * @param[in] val New value for the OCR1B
 *
 * Write this 16 bit register in an interrupt save manner
 */
static void set_reference_event(const uint16_t val)
{
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		OCR1B = val;
	}
}

/**
 * Read the timer value in a save manner
 * @return Current value from the TCNT1
 *
 * Read this 16 bit register in an interrupt save manner
 */
static uint16_t read_timer_value(void)
{
	uint16_t tmp;
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		tmp = TCNT1;
	}
	return tmp;
}

/**
 * Calculate the difference between two timestamps (limited to 16 bit)
 * @param[in] start_time time stamp at the beginning
 * @param[in] stop_time time stamp at the end
 * @return difference
 *
 * We must handle two independent and asynchron timings. This will bring in
 * some errors we must deal with:
 *
 * timer | 0001 | ------------- | B71C |
 * event ______XXXXXXXXXXXXXXXXXXX________     interval larger than 0xB71A, but smaller than 0xB71B
 *
 * event ___XXXXXXXXXXXXXXXXXXXXXXXX______     interval equals to 0xB71B
 *
 * event _XXXXXXXXXXXXXXXXXXXXXXXXXXXXX___     interval greater than 0xB71B
 *
 * If we read the time stamps 0x0001 and 0xB71C like in this example, we only
 * know the event signal takes at least 0xB71A and at most 0xB71C clocks.
 *
 * We assuming here we receive a perfect "one second signal" to compare our
 * own clock reference with. If our own reference clock is:
 *
 * - below 12.0 MHz the stamps are tending to a 0xB71A...0xB71B interval
 * - exact at 12.0 MHz the stamps are tending to a 0xB71B interval (=12E6 / 256)
 * - above 12.0 MHz the stamps are tending to a 0xB71B...0xB71C interval
 */
uint16_t calc_count_diff(const uint16_t start_time, const uint16_t stop_time)
{
	if (stop_time < start_time)
		return (0xFFFF - start_time) + stop_time + 1;

	return stop_time - start_time;
}

/**
 * Calculate the interval count average over DCF77 300 periods
 * @param[in] next_interval the next measured interval
 * @return 0 (=invalid) or the current average interval
 *
 * The average is calculated over 300 periods. Until this count is reached, we
 * return COUNT_PER_SECOND to not change anything. If this count is reached
 * we return the current average interval.
 *
 * @NOTE The callee must ensure the next_interval must be a valid one. Do not
 * insert invalid numbers here (way to small or to large intervals)
 *
 * TODO: The following must be checked...
 * - If the interval tends to an interval below the ideal one we must round
 *   up the calculated average.
 * - If the interval tends to be the ideal value, do nothing special
 * - If the interval tends to an interval above the ideal one, we must round
 *   down the calculated average.
 */
static uint16_t calc_average_interval(const uint16_t next_interval)
{
	uint16_t tmp;

	if (var.average_interval_count >= INTERVAL_SUM_COUNT_MAX) {
	/* remove one average interval from the sum */
		tmp = (var.interval_sum + (INTERVAL_SUM_COUNT_MAX >> 1)) / INTERVAL_SUM_COUNT_MAX;
		var.interval_sum -= tmp;
		var.average_interval_count --;
	}

/* next valid sample arrives */
	var.interval_sum += next_interval;
	var.average_interval_count++;

	if (var.average_interval_count < INTERVAL_SUM_COUNT)
		return COUNT_PER_SECOND;

/* first we try with a rounded down value */
	tmp = var.interval_sum / var.average_interval_count;

/* if we are in luck... */
	if (tmp >= COUNT_PER_SECOND)
		return tmp;

/* otherwise we must round up */
	return (var.interval_sum + (var.average_interval_count >> 1)) / var.average_interval_count;
}

/**
 * Calculate the average phase difference the DCF77 signal and our internal reference has
 * @param[in] next_diff next calculated difference between DCF77 and reference
 * @return 0 = do not change anything, other values if a correction is needed
 *
 * @NOTE We must use only a small amount of samples. Problem is the phase error:
 * It yields into a sum of all small errors over the time (integral). We also do
 * a sum of errors to compensate the error itself. But you can't compensate
 * the integral's result with an intergral... But I see no other solution than
 * a low pass filter to compensate the jitter.
 *
 * If the average phase value is
 * - smaller than PHASE_RESERVE: we must enlarge one interval by returning a positive value
 * - larger than PHASE_RESERVE: we must shorten one interval by returning a negative value
 */
static int16_t average_phase_value(const uint16_t next_diff)
{
	uint32_t tmp;
	int32_t ret;

#ifdef CALIBRATE
	current_phase_diff = next_diff;
#endif
	if (var.average_interval_count < INTERVAL_SUM_COUNT)
		return 0;

	if (var.average_phase_count >= PHASE_SUM_COUNT) {
		tmp = var.average_phase_diff / var.average_phase_count;
		var.average_phase_diff -= tmp;
		var.average_phase_count--;
	}

	var.average_phase_diff += next_diff;
	var.average_phase_count++;

	if (var.average_phase_count < PHASE_SUM_COUNT)
		return 0;

	ret = var.average_phase_diff / var.average_phase_count;
	ret = PHASE_RESERVE - ret;
	return ret / 50;
}

/**
 * Simple calculation of the averade phase
 * @return current averade interval
 */
uint16_t get_average_phase_value(void)
{
	if (var.average_phase_count < PHASE_SUM_COUNT)
		return 0;
	return (uint16_t)(var.average_phase_diff / var.average_phase_count);
}

/**
 * Simple calculation of the average interval
 * @return current interval length
 */
uint16_t get_average_interval_value(void)
{
	return COUNT_PER_SECOND + var.period_correction;
}

/**
 * Detect what was happened since last query
 *
 * Polls:
 * - edge (both) detector for the DCF77 signal
 * - timer compare match
 *
 * The real timer event (our reference signal) is handled atomically in an interrupt routine
 */
static void detect_events(void)
{
	uint8_t tmp;

/* no DCF77 edge within a second */
	if (TIFR & 0x10) {
		TIFR = 0x10;	/* clear the OCF1A flag (*and* do not clear other flags! E.g. do not use |= here! */
		set_status_bits(MISSED_DCF_EDGE);
	}

	tmp = ACSR;

/* detect a regular edge */
	if (tmp & 0x10) {	/* edge happened */
		ACSR |= 0x10;	/* clear the flag first */

	/* does the timer also "feels" an edge? */
		if (! (TIFR & 0x20))
			set_status_bits(NEW_BIT_IS_GARBAGE);

	/* ACO is '1' if
	 * - negative input is lower than the positive input
	 *   -> DCF is 0V, ref is 2.5V
	 *   -> if we see this bit set, the rising edge was the source
	 *
	 * ACO is '0' if
	 * - negative input is higher than the positive input
	 *   -> DCF is 5V, ref is 2.5V
	 *   -> if we see this bit reset, the falling edge was the source
	 *
	 * To avoid any garbare to confuse us, check if the last edge was the one we expected!
	 * This filters out some signal bursts.
	 */
		if (TCCR1B & 0x40) {
		/* we are programmed to detect the rising edge of ACO */
			if (tmp & 0x20) {
				/* event was a rising edge of ACO, e.g. falling edge on the DCF signal -> start of a second */
				TCCR1B &= ~0x40;	/* next capture at the falling edge of ACO */
				set_status_bits(NEXT_SECOND_STARTS);
			}
		/* else ignore this event */
		} else {
		/* we are programmed to detect the falling edge of ACO */
			if (!(tmp & 0x20)) {
				/* event was a falling edge of ACO, e.g. rising edge on the DCF signal -> end of bit per second */
				TCCR1B |= 0x40;	/* next capture at the rising edge of ACO */
				set_status_bits(NEXT_BIT_ARRIVED);
			}
		/* else ignore this event */
		}
	/*
	 * clear the ICF1 flag after changing the edge (to follow the
	 * datasheet). (Note: Do not |= here!)
	 */
		TIFR = 0x20;
	}
}

/**
 * Handle the reference timer event
 *
 * When a new reference second starts, we
 * - calculate the phase difference if the dcf egde was already happend
 * - must setup the next interrupt time
 */
static inline void handle_reference_event(void)
{
	uint16_t diff;

	if (var.cycle_state & DCF_EDGE_HAPPEND) {
		diff = calc_count_diff(var.dcf77_start_pulse_stamp, read_second_start_time_stamp());
		var.phase_correction = average_phase_value(diff);
		var.cycle_state &= ~(DCF_EDGE_HAPPEND | REF_EDGE_HAPPEND);	/* we consumed both events FIXME only one! */
#ifdef CALIBRATE
		current_phase_correction = var.phase_correction;
	} else
		current_phase_correction = 0;
		new_val = 1;
#else
	}
#endif
/*
 * we are now awaiting one new DCF edge for this cycle
 */
	var.cycle_state |= AWAITING_DCF_EDGE;

	set_reference_event(read_second_start_time_stamp() + get_average_interval_value() + var.phase_correction);
/*
 * We consumed the phase correction for _this_ cycle
 */
	var.phase_correction = 0;
/*
 * give a simple visible feedback
 */
	enable_red_led();
}

/**
 * Handle the dcf77 pulse event
 *
 * We only accepting one DCF edge per cycle. More than this will be ignored.
 * This sorts out most bad edges. But still not all
 *
 * The timeout detection must be re-programmed on each of this edges. We can't
 * guess which one is the correct one.
 *
 * Mark the timeout detection as armed, to be able to detect bad reception
 * over a longer interval.
 *
 * Enable the yellow LED on each event. This makes it easier to give the user
 * a visible feedback of the current reception.
 */
static inline void handle_dcf_event(void)
{
	uint16_t diff;

	var.dcf77_old_start_pulse_stamp = var.dcf77_start_pulse_stamp;
	var.dcf77_start_pulse_stamp = read_dcf_edge_stamp();

/* re enable timeout detection */
	set_timeout_detection(var.dcf77_start_pulse_stamp + get_average_interval_value() + 2000);

	var.cycle_state |= DCF_TIMEOUT_ARMED;
	enable_yellow_led();

	if (var.cycle_state & AWAITING_DCF_EDGE) {

		var.cycle_state |= DCF_EDGE_HAPPEND;
		var.cycle_state &= ~AWAITING_DCF_EDGE;	/* done for this cycle */
	/*
	 * Do the average interval calculation
	 */
		if (var.cycle_state & LAST_DCF_EDGE_HAPPEND) {	/* did we receive a 2nd event? */
			diff = calc_count_diff(var.dcf77_old_start_pulse_stamp, var.dcf77_start_pulse_stamp);
#ifdef CALIBRATE
			new_period_value = diff;
#endif
		/* if this interval seems okay, we can use it to calculate the average interval */
			if (diff > INTERVAL_TOO_SHORT && diff < INTERVAL_TOO_LONG) {
				var.period_correction = calc_average_interval(diff) - COUNT_PER_SECOND;
			}
		} else
			var.cycle_state |= LAST_DCF_EDGE_HAPPEND;	/* we have at least one valid event */
	} else {
	/*
	 * more than one DCF signal per cycle. We do not know which one
	 * is the correct one. So its better to ignore them all
	 */
		var.cycle_state &= ~(LAST_DCF_EDGE_HAPPEND | DCF_EDGE_HAPPEND);
	}
}

/**
 * Handle the end of the DCF impulse
 *
 * The end of the impulse is only recognized when also a start event happend.
 * Each impulse is sorted out if it seems to short or too long. If this happens
 * no further calculation of the phase or the interval lenght is done in this
 * cycle.
 */
static inline void handle_bit_event(void)
{
	uint16_t diff;

	var.dcf77_stop_pulse_stamp = read_dcf_edge_stamp();

	/* this event should not happen before the NEXT_SECOND_STARTS event! */
	if (var.cycle_state & DCF_EDGE_HAPPEND) {
		diff = calc_count_diff(var.dcf77_start_pulse_stamp, var.dcf77_stop_pulse_stamp);
		if (diff < PULSE_TOO_SMALL || diff > PULSE_TOO_WIDE) {
			report_bit(BIT_UNKNOWN);
			DEBUGC('?');
		/* don't use this interval for any further calculation */
			var.cycle_state &= ~(DCF_EDGE_HAPPEND | REF_EDGE_HAPPEND);
		} else {
			if (diff > PULSE_SHORT) {
				report_bit(BIT_1);
				DEBUGC('1');
			} else {
				report_bit(BIT_0);
				DEBUGC('0');
			}
		}
#ifdef CALIBRATE
	} else
		DEBUGC('X');
#else
	}
#endif

	disable_yellow_led();
}

void handle_events(void)
{
	uint8_t status;
	uint16_t diff;

	detect_events();

/* ignore one bit, its for USB usage only */
	status = timer_status & ~NEW_SECOND_STARTS;

/* loop until every event was handled */
	while (status) {

	/*
	 * Note: This must always checked first!
	 *
	 * No dcf edge happens within a little more than 1 second
	 * than the last one. This means:
	 * - bad receiption (also an edge storm was sorted out)
	 * - the sync marker
	 * We are optimistic and handle it as the sync marker
	 */
		if (status & MISSED_DCF_EDGE) {
			reset_status_bits(MISSED_DCF_EDGE);
		/* DCF77 impulse expired */
			set_timeout_detection(read_timeout_detection() + get_average_interval_value());

			if (var.cycle_state & DCF_TIMEOUT_ARMED) {
				report_bit(BIT_SYNC);
				DEBUGC('S');
			} else {
			/*
			 * More than one period without a valid DCF signal.
			 * At least reset the phase detection
			 */
				var.average_phase_count = 0;
			}
		/*
		 * There are no more valid edges present. Everything must start again.
		 * This avoids any miscalculation of the interval and the phase
		 */
			var.cycle_state &= ~(DCF_EDGE_HAPPEND | REF_EDGE_HAPPEND | LAST_DCF_EDGE_HAPPEND | AWAITING_DCF_EDGE | DCF_TIMEOUT_ARMED);
		}

	/*
	 * First check if this bit is to be ignored!
	 * If yes: Reset all saved events
	 */
		if (status & NEW_BIT_IS_GARBAGE) {
			reset_status_bits(NEW_BIT_IS_GARBAGE | NEXT_SECOND_STARTS | NEXT_BIT_ARRIVED);
			var.cycle_state &= ~(DCF_EDGE_HAPPEND | REF_EDGE_HAPPEND | LAST_DCF_EDGE_HAPPEND | AWAITING_DCF_EDGE);
		}

		if (status & TIMER_REFERENCE) {
			reset_status_bits(TIMER_REFERENCE);
			handle_reference_event();
		}

		if (status & NEXT_SECOND_STARTS) {
			reset_status_bits(NEXT_SECOND_STARTS);
			handle_dcf_event();
		}

		if (status & NEXT_BIT_ARRIVED) {
			reset_status_bits(NEXT_BIT_ARRIVED);
			handle_bit_event();
		}

	/* ignore one bit, its for USB usage only */
		status = timer_status & ~NEW_SECOND_STARTS;
	}

	diff = calc_count_diff(read_second_start_time_stamp(), read_timer_value());
	if (diff > 5000)
		disable_red_led();
}

#ifdef CALIBRATE
void print_calculated_values(void)
{
	if (new_val) {
		new_val = 0;
#if 1
		printf("\ni: %d (%lX,%04hX) p: %hu|%hd ", var.period_correction, var.interval_sum, var.average_interval_count, current_phase_diff, current_phase_correction);
#else
		printf("\n<%04hx,%04hx>", new_period_value, current_phase_diff);
#endif
		/* show only new values */
		new_period_value = 0;
		current_phase_diff = 0;
	}
}
#endif

/* end of file event_handler.c */
